package com.ramon.twgerenciadortarefas.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ramon.twgerenciadortarefas.modelos.Usuario;

public interface RepositorioUsuario extends JpaRepository<Usuario, Long>{

	Usuario findByEmail(String email);
}
